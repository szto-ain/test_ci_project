from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "django_docker_gitlab.users"
    verbose_name = _("Users")

    def ready(self):
        try:
            import django_docker_gitlab.users.signals  # noqa F401
        except ImportError:
            pass
